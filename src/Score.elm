module Score exposing
    ( Score
    , initEmpty
    , initWithServe
    , update
    , view
    )

-- MODEL

type Serve
    = No
    | First
    | Second


type alias Score =
    { points : Int
    , serve : Serve
    }


initEmpty : Score
initEmpty =
    { points = 0, serve = No }


initWithServe : Score
initWithServe =
    { points = 0, serve = Second }


-- UPDATE

update : Score -> Score -> (Score, Score)
update won fault =
    case (won.serve, fault.serve) of
        (No, Second) ->
            ({ won | serve = First }, { fault | serve = No })

        (No, First) ->
            ( won, { fault | serve = Second })

        (_, No) ->
            ( { won | points = won.points + 1 }, fault)

        (_, _) ->
            ( won, fault )


-- VIEW

view : Score -> String
view {points, serve} =
    let
        pointsString = toString points

        serveString =
            case serve of
                No -> ""
                First -> "."
                Second -> ":"

    in
        pointsString ++ serveString
