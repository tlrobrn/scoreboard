module Scoreboard exposing (Scoreboard, Msg, init, update, view)

import Score exposing (Score)
import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)


-- MODEL

type alias Scoreboard =
    { leftList : List Score
    , rightList : List Score
    , complete : Bool
    , updatedList : List Updated
    }


type Updated
    = ULeft
    | URight
    | UBoth


init : Scoreboard
init =
    { leftList = [ Score.initWithServe ]
    , rightList = [ Score.initEmpty ]
    , complete = False
    , updatedList = []
    }

currentScore : List Score -> Score
currentScore scores =
    Maybe.withDefault Score.initEmpty (List.head scores)

-- UPDATE

type Msg
    = Left
    | Right
    | Switch
    | Undo


update : Msg -> Scoreboard -> Scoreboard
update msg scoreboard =
    let
        {leftList, rightList, complete, updatedList} = scoreboard
        left = currentScore leftList
        right = currentScore rightList
    in
        if complete && msg /= Undo then
            scoreboard
        else
            case msg of
                Left ->
                    let
                        (newLeft, newRight) = Score.update left right
                        updateLeft = newLeft /= left
                        updateRight = newRight /= right

                    in
                        updateScores (newLeft, newRight) (updateLeft, updateRight) scoreboard

                Right ->
                    let
                        (newRight, newLeft) = Score.update right left
                        updateLeft = newLeft /= left
                        updateRight = newRight /= right

                    in
                        updateScores (newLeft, newRight) (updateLeft, updateRight) scoreboard

                Switch ->
                    { leftList = rightList
                    , rightList = leftList
                    , complete = complete
                    , updatedList = List.map switch updatedList
                    }

                Undo ->
                    case updatedList of
                        [] -> scoreboard
                        _ -> undo scoreboard


updateScores : (Score, Score) -> (Bool, Bool) -> Scoreboard -> Scoreboard
updateScores (newLeft, newRight) updates scoreboard =
    let {leftList, rightList, complete, updatedList} = scoreboard
    in
        case updates of
            (True, False) ->
                { scoreboard
                | leftList = newLeft::leftList
                , complete = isComplete newLeft newRight
                , updatedList = ULeft::updatedList
                }

            (False, True) ->
                { scoreboard
                | rightList = newRight::rightList
                , complete = isComplete newLeft newRight
                , updatedList = URight::updatedList
                }

            (True, True) ->
              { leftList = newLeft::leftList
              , rightList = newRight::rightList
              , complete = isComplete newLeft newRight
              , updatedList = UBoth::updatedList
              }

            (False, False) ->
                scoreboard


isComplete : Score -> Score -> Bool
isComplete left right =
    let
        leftPoints = left.points
        rightPoints = right.points
    in
        (max leftPoints rightPoints) >= 11
            && (abs (leftPoints - rightPoints)) >= 2


undo : Scoreboard -> Scoreboard
undo scoreboard =
    let
        {leftList, rightList, complete, updatedList} = scoreboard
        leftTail = Maybe.withDefault [] (List.tail leftList)
        rightTail = Maybe.withDefault [] (List.tail rightList)
    in
        case updatedList of
            ULeft::eventTail ->
                { leftList = leftTail
                , rightList = rightList
                , complete = isComplete (currentScore leftTail) (currentScore rightList)
                , updatedList = eventTail
                }

            URight::eventTail ->
                { leftList = leftList
                , rightList = rightTail
                , complete = isComplete (currentScore leftList) (currentScore rightTail)
                , updatedList = eventTail
                }

            UBoth::eventTail ->
                { leftList = leftTail
                , rightList = rightTail
                , complete = isComplete (currentScore leftTail) (currentScore rightTail)
                , updatedList = eventTail
                }

            _ ->
                scoreboard


switch : Updated -> Updated
switch update =
    case update of
        ULeft -> URight
        URight -> ULeft
        _ -> update

-- VIEW

view : Scoreboard -> Html Msg
view scoreboard =
    let
        left = currentScore scoreboard.leftList
        right = currentScore scoreboard.rightList
    in
        div []
            [ button [ onClick Left ] [ text (Score.view left) ]
            , button [ onClick Right ] [ text (Score.view right) ]
            , button [ onClick Switch ] [ text "Switch" ]
            , button [ onClick Undo ] [ text "Undo" ]
            , div [] [ text (gameOver scoreboard) ]
            ]


gameOver : Scoreboard -> String
gameOver {leftList, rightList, complete, updatedList} =
    if complete then
        let
            left = currentScore leftList
            right = currentScore rightList
            winner =
                if left.points > right.points then
                    "Left"
                else
                    "Right"

        in
            winner ++ " wins!"
    else
        ""
