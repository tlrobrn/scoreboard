# Scoreboard

A simple scoreboard implementation in Elm.

This was designed for pickleball, as such it is designed for games to 11 won by 2.


### Icon
_scoreboard by Iain Hector from the Noun Project_
