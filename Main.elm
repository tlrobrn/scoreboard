import Scoreboard
import Html.App as App

main : Program Never
main =
    App.beginnerProgram
        { model = Scoreboard.init
        , update = Scoreboard.update
        , view = Scoreboard.view
        }
